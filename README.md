Description
-----------

Ansible role which installs and configures Graylog log management.

**THIS ROLE IS FOR GRAYLOG-3.X ONLY! FOR OLDER VERSIONS USE THE `GRAYLOG-2.X` BRANCH!**

Dependencies
------------

- **Only Ansible versions > 2.2.0 are supported.**
- [Elasticsearch][1]
- [NGINX][2]
- Tested on Centos 7

Quickstart
----------

- You need at least 4GB of memory to run Graylog
- Run `molecule converge -s graylog-3.0.2`
- Login to Graylog by opening `http://127.0.0.1:9000` in your browser. Default username and password is `admin`

Required Variables
--------

```yaml
# Basic server settings
graylog_password_secret:    "2jueVqZpwLLjaWxV" # generate with: pwgen -s 96 1
graylog_root_password_sha2: "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918"
graylog_http_publish_uri: "http://{{ ansible_default_ipv4.address }}:9000/"
graylog_http_external_uri: "http://{{ ansible_default_ipv4.address }}:9000/"
```

Take a look into `defaults/main.yml` to get an overview of all configuration parameters.

More detailed example
---------------------

```yaml
- hosts: "server"
  become: True
  vars:
    es_instance_name: "graylog"
    es_scripts: False
    es_templates: False
    es_version_lock: False
    es_heap_size: "1g"
    es_config:
      node.name: "graylog"
      cluster.name: "graylog"
      http.port: 9200
      transport.tcp.port: 9300
      network.host: "127.0.0.1"
      node.data: True
      node.master: True
    graylog_install_java: False # Elasticsearch role already installed Java
    graylog_password_secret: "2jueVqZpwLLjaWxV" # generate with: pwgen -s 96 1
    graylog_root_password_sha2: "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918"
    graylog_http_bind_address: "{{ ansible_default_ipv4.address }}:9000"
    graylog_http_publish_uri: "http://{{ ansible_default_ipv4.address }}:9000/"
    graylog_http_external_uri: "http://{{ ansible_default_ipv4.address }}:9000/"

    nginx_sites:
      graylog:
        - "listen 80"
        - "server_name graylog"
        - |
          location / {
            proxy_pass http://localhost:9000/;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass_request_headers on;
            proxy_connect_timeout 150;
            proxy_send_timeout 100;
            proxy_read_timeout 100;
            proxy_buffers 4 32k;
            client_max_body_size 8m;
            client_body_buffer_size 128k;
          }

  roles:
    - role: "Graylog2.graylog-ansible-role"
      tags:
        - "graylog"
```

- Run the playbook with `ansible-playbook -i inventory_file your_playbook.yml`
- Login to Graylog by opening `http://<host IP>` in your browser, default username and password is `admin`

Tests
-----

Tests are ran using `molecule test --all`

Further Reading
----------------

Great articles by Pablo Daniel Estigarribia Davyt on how to use this role:

- [Install Graylog][4]
- [Receive messages from Logstash][5]
- [Monitor Graylog with NSCA][6]

License
-------

Author: Marius Sturm (<marius@graylog.com>) and [contributors][3]

License: Apache 2.0

[1]: https://github.com/elastic/ansible-elasticsearch
[2]: https://github.com/jdauphant/ansible-role-nginx
[3]: https://github.com/Graylog2/graylog2-ansible-role/graphs/contributors
[4]: https://pablodav.github.io/post/graylog/graylog_ansible
[5]: https://pablodav.github.io/post/graylog/logstash_input
[6]: https://pablodav.github.io/post/graylog/graylog_logstash_nagios_nsca
