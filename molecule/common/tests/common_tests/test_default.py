import json
import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('graylog')


def test_graylog_server_running_and_enabled(host):
    graylog = host.service("graylog-server")
    assert graylog.is_enabled
    assert graylog.is_running


@pytest.mark.parametrize(
    "package", [
        ("graylog-server")
    ]
)
def test_graylog_package_version(package, graylog_version, host):

    package = host.package(package)

    assert package.version.startswith(graylog_version)
    assert package.is_installed


def test_graylog_web_api(host, graylog_version, graylog_web_scheme):
    """
    Ensure API returns something.
    """

    response = host.run((
            f"curl {graylog_web_scheme}://127.0.0.1:9000/api "
            f"-H 'Accept: application/json' "
            f"-X GET"
            ))

    assert json.loads(
        response.stdout
        )['version'].startswith(graylog_version)
