import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('graylog')


def test_ssl_graylog(host):

    result = host.run(
        'openssl s_client -connect localhost:9000 <<< "Q"'
    ).stdout

    assert 'Secure Renegotiation IS supported' in result
